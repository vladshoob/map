# Innovecs test task

Task: simple API to validate address. As minimalistic as possible using Laravel. Entry data: just address.

Notes:
- / (index) returns dummy page just to be sure app is working;
- POST to /api/validate validates address via Google Geocoding API;
- validation returns { success: false } with validation errors if smth wrong with entry;
- returns { success: false } if address is not valid;
- returns { success: true, data: [meta] } if validation successful;
- for local tests please fill .env with GOOGLE_MAPS_GEOCODING_API_KEY provided separately;

What can be done more:
- to reduce API load we might need some sort of caching or / and database entry;
- to avoid overuse of our API we might add throttle middleware.
- we might make AddressValidatorInterface if more providers will be developed.
<?php

namespace Tests\Feature;

use App\Services\ValidatorService;
use Illuminate\Support\Str;
use Tests\TestCase;

class ValidatorTest extends TestCase
{
    /** @test */
    public function api_validator_endpoint_requires_address_field()
    {
        $this->withExceptionHandling();

        $this->post(route('api.validate'), [])
            ->assertStatus(422)
            ->assertJson(['success' => false])
            ->assertJsonStructure(
                ['data' => ['address']]
            );
    }

    /**
     * @group web
     * @test
     */
    public function api_validator_returns_meta_data_for_existing_address()
    {
        $this->post(route('api.validate'), ['address' => 'Lomonosova 59, Kyiv, Ukraine'])
            ->assertOk()
            ->assertJson(['success' => true])
            ->assertJsonStructure(
                ['data' => ['lat', 'lng']]
            );
    }

    /** @test */
    public function api_validator_returns_error_for_invalid_address()
    {
//        $this->mock(ValidatorService::class, function ($mock) {
//            $mock->shouldReceive('validate')
//                ->once()
//                ->andReturn(False); // always return false
//        });

        $this->post(route('api.validate'), ['address' => Str::random()])
            ->assertStatus(404)
            ->assertJson(['success' => false]);
    }
}

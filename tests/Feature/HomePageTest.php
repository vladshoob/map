<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomePageTest extends TestCase
{
    /** @test */
    public function ensure_main_web_page_is_accessible()
    {
        $this->get('/')->assertOk();
    }
}

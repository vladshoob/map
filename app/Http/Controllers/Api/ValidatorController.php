<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ValidatorRequest;
use App\Services\ValidatorService;

class ValidatorController extends BaseController
{
    /**
     * Invokable method to validate address.
     *
     * @param ValidatorRequest $request
     * @param ValidatorService $service
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ValidatorRequest $request, ValidatorService $service)
    {
        if ($request->errors)
            return $this->sendError('Error', $request->errors, 422);

        if (!$data = $service->validate($request->validated()['address']))
            return $this->sendError('Not Found', [], 404);

        return $this->sendResponse($data, 'Success');
    }
}

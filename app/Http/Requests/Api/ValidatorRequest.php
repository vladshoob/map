<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ValidatorRequest extends FormRequest
{
    /**
     * Errors array, if any.
     *
     * @var
     */
    public $errors;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|string|max:1000',
        ];
    }

    /**
     * Method overwrite:
     * decide controller to decide how to send response.
     *
     * @param Validator $validator
     * @void
     */
    protected function failedValidation(Validator $validator)
    {
        $this->errors = $validator->errors()->toArray();
    }
}

<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Spatie\Geocoder\Geocoder;

class GoogleGeocoderValidator // implements AddressValidatorInterface
{
    protected $geocoder;

    public function __construct()
    {
        $client = new Client(['verify' => false]);

        $this->geocoder = (new Geocoder($client))
                            ->setApiKey(config('geocoder.key'));
    }

    /**
     * Validates address with Google Geocoder API.
     *
     * @param string $address
     * @return bool|array
     */
    public function validate(string $address)
    {
        try {

            $response = $this->geocoder->getCoordinatesForAddress($address);

            if ($response['accuracy'] == 'result_not_found')
                return False;

            return $response;

        } catch (\Error $e) {

            Log::error(json_encode($e));
            return False;

        }
    }
}

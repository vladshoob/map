<?php

namespace App\Services;

class ValidatorService
{
    /**
     * Address validator implementation.
     *
     * @var
     */
    protected $validator;

    /**
     * ValidatorService constructor.
     *
     * @param GoogleGeocoderValidator $validator
     */
    public function __construct(GoogleGeocoderValidator $validator) {
        $this->validator = $validator;
    }

    /**
     * Delegates address validation.
     * Returns meta data or False statement.
     *
     * @param string $address
     * @return array|bool
     */
    public function validate(string $address)
    {
        return $this->validator->validate($address);
    }
}
